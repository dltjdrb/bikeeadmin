/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.dashboard')
      .directive('dashboardTable', dashboardTable);

  /** @ngInject */
  function dashboardTable() {
    return {
      restrict: 'E',
      controller: 'TablesPageCtrl',
      templateUrl: 'app/pages/dashboard/tables/dashboardTable.html'
    };
  }
})();