/**
 * Created by LSK on 2016-05-24.
 */
/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.dashboard')
      .controller('DashboardCalendarCtrl', DashboardCalendarCtrl);

  /** @ngInject */
  function DashboardCalendarCtrl(baConfig, ngDialog, $http , $scope,$window,$document) {
    var dashboardColors = baConfig.colors.dashboard;
    var reserves = $scope.reserves = [];
    $http.get("https://api.bikee.kr/reserves")
        .success(function(result) {
          var result = result.result;
          console.log(result);
          angular.forEach(result,function(result){
            switch (result.reserve.status){
              case "RR" : console.log('RR');
                reserves.push({
                  start : result.reserve.rentStart,
                  end : result.reserve.rentEnd,
                  title : result.bike.title,
                  color: "red"
                })
                break;
              case "RS" :
                reserves.push({
                  start : result.reserve.rentStart,
                  end : result.reserve.rentEnd,
                  title : result.bike.title,
                  color: dashboardColors.silverTree
                });
                break;
              case "LC" : console.log("LC");
                reserves.push({
                  start : result.reserve.rentStart,
                  end : result.reserve.rentEnd,
                  title : result.bike.title,
                  color: dashboardColors.surfieGreen
                });
                break;
              default :
                reserves.push({
                  start : result.reserve.rentStart,
                  end : result.reserve.rentEnd,
                  title : result.bike.title,
                  color: dashboardColors.white
                });
                break;
            }
          });
          init();
        })
    function init(){
      var $element = $('#calendar').fullCalendar({
        //height: 335,
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
        defaultDate: '2016-05-08',
        selectable: true,
        draggable:false,
        selectHelper: false,
        lang:"ko",
        eventClick:function(calEvent,jsEvent,view){
         /* $scope.$on('ngDialog.opened', function (event, $dialog) {
            console.log($dialog);
            var height = $(".ngdialog-content").height();
            var width = $(".ngdialog-content").width();
            var top = jsEvent.pageY/2 -20 +"px";
            var left = jsEvent.pageX/2+"px";
            $(".ngdialog-content").css("margin-top",top);
            $(".ngdialog-content").css("margin-left",left);
          })
          console.log('Event: ' + calEvent.title);
          console.log('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
          console.log('View: ' + view.name);

          // change the border color just for fun
          $(this).css('border-color', 'red');*/
        },
        eventRender:function(ev,ele){
          ele.click(function(){
            ngDialog.open({  template: "app/pages/dashboard/dashboardCalendar/test.html",appendTo:false,showClose:true,
              controller: ['$scope', function($scope, otherService) {

                $scope.start = moment(ev.start).format('MMM Do h:mm A');
                $scope.end = moment(ev.end).format('MMM Do h:mm A');
                $scope.title = ev.title;
                console.log($scope.start);
                // controller logic
              }],className: 'ngdialog-theme-default' });

          })
        },
        select: function (start, end) {
          var title = prompt('Event Title:');
          var eventData;
          if (title) {
            eventData = {
              title: title,
              start: start,
              end: end
            };
            $element.fullCalendar('renderEvent', eventData, true); // stick? = true
          }
          $element.fullCalendar('unselect');
        },
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: reserves
      });
    }

  }
})();