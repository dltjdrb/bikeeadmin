/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.dashboard')
      /*.controller('DashboardMapCtrl', DashboardMapCtrl);*/
      .controller('GmapPageCtrl', GmapPageCtrl);



  function GmapPageCtrl($timeout,$http,$scope) {
    var serverUrl = "https://api.bikee.kr";
    function initialize() {
      var vm = this;
      var infowindow =  new google.maps.InfoWindow();;
      var current = new google.maps.Marker();
      var map = null;
      var lat,lng,bikeList;
      var latlng = [];
      var markers = [];
      var list = $scope.list = {};
      $scope.image= "http://restapi.fs.ncloud.com/bikee-image/icon_bikee.png";
      var Icon = new google.maps.MarkerImage(
          "http://restapi.fs.ncloud.com/bikee-image/icon_bikee.png" ,
          null, /* size is determined at runtime */
          null, /* origin is 0,0 */
          null, /* anchor is bottom center of the scaled image */
          new google.maps.Size(55, 23)
          /*new google.maps.Size(10, 10)*/
      );
      var mapCanvas = document.getElementById('amChartMap');
      var mapOptions = {
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      //map = new google.maps.Map(mapCanvas, mapOptions);
    /*  var mapCanvas = document.getElementById('amChartMap');
      var mapOptions = {
        center: new google.maps.LatLng(37.4776006,127.1244764),
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var map = new google.maps.Map(mapCanvas, mapOptions);*/


      if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          lat = pos.lat;
          lng = pos.lng;
          bikeList = "https://api.bikee.kr/bikes/map/"+lng+"/"+lat;
          //bikeList = "https://api.bikee.kr/bikes";
          //bikeList = serverUrl+"/bikes"
          /*bikeList = "http://52.79.73.201:3000/bikes/all/"+lng+"/"+lat;*/

          mapOptions.center = {lat: pos.lat, lng: pos.lng};
          map = new google.maps.Map(mapCanvas, mapOptions);
          current.setPosition(pos);
          current.setMap(map);
          /*infowindow.setPosition(pos);
           infowindow.setContent('Location found.');*/
          map.setCenter(pos);

          $http({
            method: 'GET',
            url: bikeList
          }).success(function(result) {
        	  console.log(result);
            //your code when success
            list = $scope.list = result.result;
            console.log('bikes ' ,result.result );
            if(list){
              for (var i = 0; i < list.length; i++) {
                markers[i] = new google.maps.Marker({ "icon":Icon});
                var latLng = new google.maps.LatLng(list[i].loc.coordinates[1]+(Math.random() -.5) /20000, list[i].loc.coordinates[0]+(Math.random() -.5) / 20000);
                markers[i].setPosition(latLng);
                markers[i].setMap(map);
                latlng.push(latLng);
                showinfoWindow(markers[i],list[i],map,infowindow);
              }
            }
            $scope.markerClusterer = new MarkerClusterer(map, markers,  {
              maxZoom: 18,
              gridSize: 40,
              styles:[{
                url: 'http://restapi.fs.ncloud.com/bikee-image/pin7.jpg',
                height: 48,
                width: 30,
                anchor: [-18, 0],
                textColor: '#333',
                textSize: 11,
                iconAnchor: [15, 48]
              }]
            });
            $scope.markerClusterer.addListener("click",function(e){
              console.log('data', e.getMarkers());
            })
          }).
              error(function(status) {
                //your code when fails
              });
        }, handleLocationError);
      } else {
        mapOptions.center = {lat: 37.466610, lng:126.960348};
        map = new google.maps.Map(mapCanvas, mapOptions);
      }

    }

    $timeout(function(){
      $http.post(serverUrl+"/users/session",{email:"admin2@naver.com",password:"1234"})
          .then(function(result){
            console.log('result ' ,result)
            initialize();
          },function(err){
            console.log('err ' ,err);
          })

    }, 100);


    function showinfoWindow(marker, list,map,infowindow){
      marker.addListener('click', function() {
        console.log('marker ' , marker.getPosition());
        //map.setCenter(marker.getPosition());
        map.panTo(marker.getPosition());
        var info = "<div ng-non-bindable=''>"+
            "<img src='http://restapi.fs.ncloud.com/bikee-image/KakaoTalk_20160404_164833385.png' height='150' width='200'>"+
            "<div>"+list.title+"</div></div>"
        infowindow.setContent(info)
        infowindow.open(map,marker);

      });
    }

    function handleLocationError(error) {
      switch(error.code) {
        case error.PERMISSION_DENIED:
          //alert("User denied the request for Geolocation.")
          alert("위치 요청 거부. GPS상태를 확인")
          break;
        case error.POSITION_UNAVAILABLE:
          alert("Location information is unavailable.")
          break;
        case error.TIMEOUT:
          alert("The request to get user location timed out.")
          break;
        case error.UNKNOWN_ERROR:
          alert("An unknown error occurred.")
          break;
      }
    }
  }



  /** @ngInject */
  function DashboardMapCtrl(layoutColors, layoutPaths) {
    var map = AmCharts.makeChart('amChartMap', {
      type: 'map',
      theme: 'blur',
      zoomControl: { zoomControlEnabled: false, panControlEnabled: false },

      dataProvider: {
        map: 'worldLow',
        zoomLevel: 3.5,
        zoomLongitude: 10,
        zoomLatitude: 52,
        areas: [
          { title: 'Austria', id: 'AT', color: layoutColors.primary, customData: '1 244', groupId: '1'},
          { title: 'Ireland', id: 'IE', color: layoutColors.primary, customData: '1 342', groupId: '1'},
          { title: 'Denmark', id: 'DK', color: layoutColors.primary, customData: '1 973', groupId: '1'},
          { title: 'Finland', id: 'FI', color: layoutColors.primary, customData: '1 573', groupId: '1'},
          { title: 'Sweden', id: 'SE', color: layoutColors.primary, customData: '1 084', groupId: '1'},
          { title: 'Great Britain', id: 'GB', color: layoutColors.primary, customData: '1 452', groupId: '1'},
          { title: 'Italy', id: 'IT', color: layoutColors.primary, customData: '1 321', groupId: '1'},
          { title: 'France', id: 'FR', color: layoutColors.primary, customData: '1 112', groupId: '1'},
          { title: 'Spain', id: 'ES', color: layoutColors.primary, customData: '1 865', groupId: '1'},
          { title: 'Greece', id: 'GR', color: layoutColors.primary, customData: '1 453', groupId: '1'},
          { title: 'Germany', id: 'DE', color: layoutColors.primary, customData: '1 957', groupId: '1'},
          { title: 'Belgium', id: 'BE', color: layoutColors.primary, customData: '1 011', groupId: '1'},
          { title: 'Luxembourg', id: 'LU', color: layoutColors.primary, customData: '1 011', groupId: '1'},
          { title: 'Netherlands', id: 'NL', color: layoutColors.primary, customData: '1 213', groupId: '1'},
          { title: 'Portugal', id: 'PT', color: layoutColors.primary, customData: '1 291', groupId: '1'},
          { title: 'Lithuania', id: 'LT', color: layoutColors.successLight, customData: '567', groupId: '2'},
          { title: 'Latvia', id: 'LV', color: layoutColors.successLight, customData: '589', groupId: '2'},
          { title: 'Czech Republic ', id: 'CZ', color: layoutColors.successLight, customData: '785', groupId: '2'},
          { title: 'Slovakia', id: 'SK', color: layoutColors.successLight, customData: '965', groupId: '2'},
          { title: 'Estonia', id: 'EE', color: layoutColors.successLight, customData: '685', groupId: '2'},
          { title: 'Hungary', id: 'HU', color: layoutColors.successLight, customData: '854', groupId: '2'},
          { title: 'Cyprus', id: 'CY', color: layoutColors.successLight, customData: '754', groupId: '2'},
          { title: 'Malta', id: 'MT', color: layoutColors.successLight, customData: '867', groupId: '2'},
          { title: 'Poland', id: 'PL', color: layoutColors.successLight, customData: '759', groupId: '2'},
          { title: 'Romania', id: 'RO', color: layoutColors.success, customData: '302', groupId: '3'},
          { title: 'Bulgaria', id: 'BG', color: layoutColors.success, customData: '102', groupId: '3'},
          { title: 'Slovenia', id: 'SI', color: layoutColors.danger, customData: '23', groupId: '4'},
          { title: 'Croatia', id: 'HR', color: layoutColors.danger, customData: '96', groupId: '4'}
        ]
      },

      areasSettings: {
        rollOverOutlineColor: '#FFFFFF',
        rollOverColor: layoutColors.primaryDark,
        alpha: 0.8,
        unlistedAreasAlpha: 0.1,
        balloonText: '[[title]]: [[customData]] users'
      },


      legend: {
        width: '100%',
        marginRight: 27,
        marginLeft: 27,
        equalWidths: false,
        backgroundAlpha: 0.5,
        backgroundColor: '#FFFFFF',
        borderColor: '#ffffff',
        borderAlpha: 1,
        top: 362,
        left: 0,
        horizontalGap: 10,
        data: [
          {
            title: 'over 1 000 users',
            color: layoutColors.primary
          },
          {
            title: '500 - 1 000 users',
            color: layoutColors.successLight
          },
          {
            title: '100 - 500 users',
            color: layoutColors.success
          },
          {
            title: '0 - 100 users',
            color: layoutColors.danger
          }
        ]
      },
      export: {
        enabled: true
      },
      creditsPosition: 'bottom-right',
      pathToImages: layoutPaths.images.amChart
    });
  }
})();