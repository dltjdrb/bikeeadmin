/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.dashboard')
      .controller('DashboardMapCtrl', GmapPageCtrl);

  /** @ngInject */
  function GmapPageCtrl($timeout,$http,$scope) {
    var serverUrl = "https://api.bikee.kr";
    //var serverUrl = "http://localhost:8080";
    function initialize() {
      var vm = this;
      var infowindow =  new google.maps.InfoWindow();;
      var current = new google.maps.Marker();
      var map = null;
      var lat,lng,bikeList;
      var latlng = [];
      var markers = [];
      var list = $scope.list = {};
      $scope.image= "http://restapi.fs.ncloud.com/bikee-image/icon_bikee.png";
      var Icon = new google.maps.MarkerImage(
          "http://restapi.fs.ncloud.com/bikee-image/icon_bikee.png" ,
          null, /* size is determined at runtime */
          null, /* origin is 0,0 */
          null, /* anchor is bottom center of the scaled image */
          new google.maps.Size(55, 23)
          /*new google.maps.Size(10, 10)*/
      );
      var mapCanvas = document.getElementById('google-maps');
      var mapOptions = {
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      //map = new google.maps.Map(mapCanvas, mapOptions);
      /*  var mapCanvas = document.getElementById('amChartMap');
       var mapOptions = {
       center: new google.maps.LatLng(37.4776006,127.1244764),
       zoom: 12,
       mapTypeId: google.maps.MapTypeId.ROADMAP
       };
       var map = new google.maps.Map(mapCanvas, mapOptions);*/
      mapOptions.center = {lat: 37.466610, lng:126.960348};
      map = new google.maps.Map(mapCanvas, mapOptions);
      current.setMap(map);
      current.setPosition(mapOptions.center );
      if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          lat = pos.lat;
          lng = pos.lng;
          /*bikeList = "https://api.bikee.kr/bikes/map/"+lng+"/"+lat;*/
          //bikeList = "https://api.bikee.kr/bikes";
          bikeList = serverUrl+"/bikes"
          /*bikeList = "http://52.79.73.201:3000/bikes/all/"+lng+"/"+lat;*/

          current.setMap(map);
          current.setPosition(pos);
          /*infowindow.setPosition(pos);
           infowindow.setContent('Location found.');*/
          map.setCenter(pos);

          $http({
            method: 'GET',
            url: bikeList
          }).success(function(result) {
            //your code when success
            list = $scope.list = result.result;
            console.log('bikes ' ,result.result );
            if(list){
              for (var i = 0; i < list.length; i++) {
                markers[i] = new google.maps.Marker({ "icon":Icon});
                var latLng = new google.maps.LatLng(list[i].loc.coordinates[1]+(Math.random() -.5) /20000, list[i].loc.coordinates[0]+(Math.random() -.5) / 20000);
                markers[i].setPosition(latLng);
                markers[i].setMap(map);
                latlng.push(latLng);
                showinfoWindow(markers[i],list[i],map,infowindow);
              }
            }
            $scope.markerClusterer = new MarkerClusterer(map, markers,  {
              maxZoom: 14,
              gridSize: 40,
              styles:[{
                url: 'http://restapi.fs.ncloud.com/bikee-image/pin7.jpg',
                height: 48,
                width: 30,
                anchor: [-18, 0],
                textColor: '#333',
                textSize: 11,
                iconAnchor: [15, 48]
              }]
            });
            $scope.markerClusterer.addListener("click",function(e){
              console.log('data', e.getMarkers());
            })
          }).
          error(function(status) {
            //your code when fails
          });
        }, handleLocationError);
      }

    }

    $timeout(function(){
      $http.post(serverUrl+"/users/session",{email:"admin2@naver.com",password:"1234"})
          .then(function(result){
            console.log('result ' ,result)
            initialize();
          },function(err){
            console.log('err ' ,err);
          })

    }, 100);


    function showinfoWindow(marker, list,map,infowindow){
      marker.addListener('click', function() {
        console.log('marker ' , marker.getPosition());
        //map.setCenter(marker.getPosition());
        map.panTo(marker.getPosition());
        var info = "<div ng-non-bindable=''>"+
            "<img src='http://restapi.fs.ncloud.com/bikee-image/KakaoTalk_20160404_164833385.png' height='150' width='200'>"+
            "<div>"+list.title+"</div></div>"
        infowindow.setContent(info)
        infowindow.open(map,marker);

      });
    }

    function handleLocationError(error) {
      switch(error.code) {
        case error.PERMISSION_DENIED:
          //alert("User denied the request for Geolocation.")
          alert("위치 요청 거부. GPS상태를 확인")
          break;
        case error.POSITION_UNAVAILABLE:
          alert("Location information is unavailable.")
          break;
        case error.TIMEOUT:
          alert("The request to get user location timed out.")
          break;
        case error.UNKNOWN_ERROR:
          alert("An unknown error occurred.")
          break;
      }
    }
  }

})();
